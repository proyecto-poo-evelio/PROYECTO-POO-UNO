<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model  
{
    protected $fillable = [
        "id",
        "nombre", 
        "apellido", 
        "cedula",
        "direccion",
        "telefono",
        "fecha_nacimiento",
        "email"
    ];
}

